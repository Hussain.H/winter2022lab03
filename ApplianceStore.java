import java.util.Scanner;
public class ApplianceStore{
	
	public static void main (String[] args){
	Scanner sc = new Scanner(System.in);
	
		Toaster[] toasters = new Toaster[4];
		
		for(int i =0; i < toasters.length; i++){
			 toasters[i] = new Toaster();
			 
			System.out.println("What sound would a Toaster make?");
			toasters[i].Noise = sc.nextLine();
			
			System.out.println("What color is the toaster?");
			toasters[i].Color = sc.nextLine();
			
			System.out.println("How strong is your Toaster? (Give a Wattage)");
			toasters[i].Power = sc.nextInt();
			
			sc.nextLine();
			
		}
		
		System.out.println(toasters[3].Noise);
		System.out.println(toasters[3].Color);
		System.out.println(toasters[3].Power);
		
		toasters[0].Ding();
		toasters[0].Toast();
		
	}
}